import random


# dummy class represeting the data model. We could hook this up directly to the sensor or a database etc
class testInputModel(object):
    def getTemperature(self):
        return random.randrange(-5, 40, 2)

    def getPrecipitation(self):
        return bool(random.getrandbits(1))

    def getAirPressure(self):
        return random.randrange(500, 1600, 2)

    def getWindSpeed(self):
        return random.randrange(0, 100, 2)
