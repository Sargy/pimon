from Model.testInputModel import testInputModel


class weatherController(object):
    RAIN_IMAGE = '../Resources/rain.jpg'
    SNOW_IMAGE = '../Resources/snow.jpg'
    SUN_IMAGE = '../Resources/sun.jpg'

    def __init__(self):
        self.model = testInputModel()

    def getTemp(self):
        return str(self.model.getTemperature())

    def getWindSpeed(self):
        return str(self.model.getWindSpeed())

    def getAirPressure(self):
        return str(self.model.getAirPressure())

    def getBackgroundImage(self):
        if self.model.getPrecipitation():
            if self.model.getTemperature() > 0:
                return self.RAIN_IMAGE
            else:
                return self.SNOW_IMAGE
        else:
            return self.SUN_IMAGE
