import pygame
import time

from Controller.weatherController import weatherController

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

pygame.init()

size = (600, 400)
screen = pygame.display.set_mode(size)

weatherData = weatherController();

pygame.display.set_caption("Pimon")

done = False

clock = pygame.time.Clock()

font = pygame.font.SysFont('Calibri', 25, True, False)

temperatureString = weatherData.getTemp()

windspeedString = weatherData.getWindSpeed()

airPressureString = weatherData.getAirPressure()

backgroundImageLocation = weatherData.getBackgroundImage()

startTime = time.time()

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    bg = pygame.image.load(backgroundImageLocation).convert()
    screen.blit(bg, (0, 0))

    font = pygame.font.SysFont('Calibri', 25, True, False)

    elapsedTime = time.time() - startTime

    if elapsedTime > 5:  # 5secs
        startTime = time.time()
        backgroundImageLocation = weatherData.getBackgroundImage()
        windspeedString = weatherData.getWindSpeed()
        airPressureString = weatherData.getAirPressure()
        temperatureString = weatherData.getTemp()

    temperatureText = font.render("Temperature: {0}c".format(temperatureString), True, BLACK)
    windspeedText = font.render("Wind-speed {0}mph".format(windspeedString), True, BLACK)
    airPressureText = font.render("Air-pressure {0}mb".format(airPressureString), True, BLACK)

    screen.blit(temperatureText, [50, 50])
    screen.blit(windspeedText, [50, 100])
    screen.blit(airPressureText, [50, 150])

    pygame.display.flip()

    clock.tick(15)

pygame.quit()
